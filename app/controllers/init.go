package controllers

import (
	"dops/app/models"
	"github.com/logpacker/mailer/pkg/client"
	gorm "github.com/revel/modules/orm/gorm/app"
	"github.com/revel/revel"
	"os"
	"path"
)

func initializeDB() {
	gorm.DB.AutoMigrate(&models.Service{}, &models.Moderator{})
	services := []*models.Service{
		&models.Service{
			Name: `Filling the virtual version 
              (LAMP)`,
			Level:        "1 level",
			Price:        54.99,
			Tarification: "One Server per month",
		},
		&models.Service{
			Name:         `Initial monitoring setup`,
			Level:        "1 level",
			Price:        48.99,
			Tarification: "One Server per month",
		},
		&models.Service{
			Name: `Raising / configuring 
              docker-daemon`,
			Level:        "1 level",
			Price:        34.99,
			Tarification: "One Server per month",
		},
		&models.Service{
			Name: `Configuring the orchestra 
              for containers`,
			Level:        "3 level",
			Price:        68.87,
			Tarification: "One Server per month",
		},
		&models.Service{
			Name: `Writing a manifest for
              kubernetes`,
			Level:        "2 level",
			Price:        27.65,
			Tarification: "One Server per month",
		},
		&models.Service{
			Name: `Writing a Dockerfile for a
              specific application`,
			Level:        "3 level",
			Price:        49.99,
			Tarification: "One Server per month",
		},
		&models.Service{
			Name:         `Installing CMS`,
			Level:        "2 level",
			Price:        54.99,
			Tarification: "One Server per month",
		},
		&models.Service{
			Name: `Writing Manifestes for 
              CMS`,
			Level:        "3 level",
			Price:        18.99,
			Tarification: "One Server per month",
		},
		&models.Service{
			Name: `Lifting and initial setting 
              of Jenkins`,
			Level:        "1 level",
			Price:        67.99,
			Tarification: "One Server per month",
		},
		&models.Service{
			Name: `Configuring automatic
              app assembly`,
			Level:        "4 level",
			Price:        54.99,
			Tarification: "One Server per month",
		},
		&models.Service{
			Name:         `Customizing CI`,
			Level:        "5 level",
			Price:        36.99,
			Tarification: "One Server per month",
		},
		&models.Service{
			Name: `Installing a monitoring
              system`,
			Level:        "5 level",
			Price:        12.99,
			Tarification: "One Server per month",
		},
		&models.Service{
			Name: `Configuring the assembly 
              of metrics`,
			Level:        "2 level",
			Price:        201.99,
			Tarification: "One Server per month",
		},
		&models.Service{
			Name: `Configuring the Log 
              Collector`,
			Level:        "4 level",
			Price:        325.99,
			Tarification: "One Server per month",
		},
		&models.Service{
			Name:         `Writing custom scripts`,
			Level:        "4 level",
			Price:        158.99,
			Tarification: "One Server per month",
		},
		&models.Service{
			Name:         `Configure Alerting`,
			Level:        "2 level",
			Price:        96.99,
			Tarification: "One Server per month",
		},
		&models.Service{
			Name: `Customization of the 
              workflow Jira`,
			Level:        "1 level",
			Price:        72.99,
			Tarification: "One Server per month",
		},
		&models.Service{
			Name: `Integration of Task 
              Manager with CI`,
			Level:        "3 level",
			Price:        85.99,
			Tarification: "One Server per month",
		},
	}
	servicesCount := 0
	gorm.DB.Model(&models.Service{}).Count(&servicesCount)
	if servicesCount == 0 {
		for _, s := range services {
			gorm.DB.Create(s)
		}
	}
}

func initializeMailer() {
	var err error
	url, foundUrl := revel.Config.String("mailer.url")
	key, foundkey := revel.Config.String("mailer.key")
	if !foundUrl || !foundkey {
		panic("mailer config")
	}
	MailerClient, err = client.New(client.Config{
		URL:    url,
		APIKey: key,
	})
	if err != nil {
		panic(err)
	}
	gopath := os.Getenv("GOPATH")
	MailerDir = revel.Config.StringDefault("mailer.dir", path.Join(gopath, "src", "dops", "mailer"))

}

func init() {
	revel.OnAppStart(initializeDB)
	revel.OnAppStart(initializeMailer)
}
