// GENERATED CODE - DO NOT EDIT
package routes

import "github.com/revel/revel"


type tMailer struct {}
var Mailer tMailer


func (_ tMailer) Unsubscribe(
		code string,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "code", code)
	return revel.MainRouter.Reverse("Mailer.Unsubscribe", args).URL
}


type tController struct {}
var Controller tController



type tTxnController struct {}
var TxnController tTxnController


func (_ tTxnController) Begin(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("TxnController.Begin", args).URL
}

func (_ tTxnController) Commit(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("TxnController.Commit", args).URL
}

func (_ tTxnController) Rollback(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("TxnController.Rollback", args).URL
}


type tStatic struct {}
var Static tStatic


func (_ tStatic) Serve(
		prefix string,
		filepath string,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "prefix", prefix)
	revel.Unbind(args, "filepath", filepath)
	return revel.MainRouter.Reverse("Static.Serve", args).URL
}

func (_ tStatic) ServeModule(
		moduleName string,
		prefix string,
		filepath string,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "moduleName", moduleName)
	revel.Unbind(args, "prefix", prefix)
	revel.Unbind(args, "filepath", filepath)
	return revel.MainRouter.Reverse("Static.ServeModule", args).URL
}


type tTestRunner struct {}
var TestRunner tTestRunner


func (_ tTestRunner) Index(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("TestRunner.Index", args).URL
}

func (_ tTestRunner) Suite(
		suite string,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "suite", suite)
	return revel.MainRouter.Reverse("TestRunner.Suite", args).URL
}

func (_ tTestRunner) Run(
		suite string,
		test string,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "suite", suite)
	revel.Unbind(args, "test", test)
	return revel.MainRouter.Reverse("TestRunner.Run", args).URL
}

func (_ tTestRunner) List(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("TestRunner.List", args).URL
}


type tApp struct {}
var App tApp


func (_ tApp) Index(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("App.Index", args).URL
}

func (_ tApp) Contact(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("App.Contact", args).URL
}

func (_ tApp) PostContact(
		contact interface{},
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "contact", contact)
	return revel.MainRouter.Reverse("App.PostContact", args).URL
}

func (_ tApp) PostOrder(
		order interface{},
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "order", order)
	return revel.MainRouter.Reverse("App.PostOrder", args).URL
}


