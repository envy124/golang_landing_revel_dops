package controllers

import (
	"dops/app/models"
	"errors"
	gormc "github.com/revel/modules/orm/gorm/app/controllers"
	"github.com/revel/revel"
)

var countries = []string{"US", "Canada", "China", "Japan"}

type App struct {
	gormc.TxnController
}

func (c App) Index() revel.Result {
	var services []models.Service
	c.Txn.Find(&services)
	return c.Render(services)
}

func (c App) Contact() revel.Result {
	return c.Render(countries)
}

func (c App) PostContact(contact models.Contact) revel.Result {
	c.Validation.Email(contact.Email)
	c.Validation.Check(contact.Note, revel.MaxSize{1024})
	c.Validation.Check(contact.Country, revel.MinSize{2}, revel.MaxSize{16})
	c.Validation.Check(contact.Name, revel.Required{}, revel.MinSize{2}, revel.MaxSize{32})
	if contact.Website != "" {
		c.Validation.URL(contact.Website)
	}
	if contact.Phone != "" {
		c.Validation.Check(contact.Phone, revel.MinSize{10})
	}
	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(App.Contact)
	}

	var moderators []models.Moderator
	c.Txn.Find(&moderators)
	if len(moderators) == 0 {
		c.Flash.Error("Cannot find any moderators to send message to")
		return c.Redirect(App.Index)
	}

	if err := SendContact(contact, moderators); err == nil {
		c.Flash.Success("Your message has been saved")
	} else {
		c.Log.Error(err.Error())
		c.Flash.Error("Error occured while saving, try again later")
	}
	return c.Redirect(App.Index)
}

func (c App) PostOrder(order models.Order) revel.Result {
	var services []models.Service

	c.Validation.Email(order.Email)
	c.Validation.Check(order.Name, revel.Required{}, revel.MinSize{2}, revel.MaxSize{32})
	if order.Phone != "" {
		c.Validation.Check(order.Phone, revel.MinSize{10})
	}

	c.Txn.Where("id in (?)", c.Params.Form["order.Services"]).Find(&services)

	if len(services) == 0 {
		c.Flash.Error("Services not found")
		return c.RenderError(errors.New("Cannot find services"))
	}

	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(App.Index)
	}

	var moderators []models.Moderator
	c.Txn.Find(&moderators)
	if len(moderators) == 0 {
		c.Flash.Error("Cannot find any moderators to send order to")
		return c.Redirect(App.Index)
	}

	if err := SendOrder(order, services, moderators); err == nil {
		c.Flash.Success("Your order has been saved")
	} else {
		c.Log.Error(err.Error())
		c.Flash.Error("Error occured while saving, try again later")
	}
	return c.Redirect(App.Index)
}
