package models

type Contact struct {
	Name    string
	Email   string
	Website string
	Country string
	Phone   string
	Note    string
}
