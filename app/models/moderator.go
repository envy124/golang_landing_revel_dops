package models

type Moderator struct {
	Id    int
	Email string
	Admin bool `gorm:"not null;default:false"`
}
