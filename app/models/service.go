package models

type Service struct {
	Id           int
	Name         string
	Level        string
	Price        float64
	Tarification string
}

type Order struct {
	Name  string
	Email string
	Phone string
}
