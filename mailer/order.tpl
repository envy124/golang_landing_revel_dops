<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DOps new order</title>
<style>
	@media (min-width: 700px){

	}
	@media (max-width: 699px){
		.letter-title {
			font-size: 18px;
		}
	}
	@media (max-width: 550px){

	}
</style>
</head>
<body style="padding:0;margin:0">
	<table cellpadding="0" cellspacing="0" width="100%" border="0" style="width: 100%; padding-top: 80px;">
		<tr style="text-align: center;">
			<td>
				<h2 class="letter-title" style="font-size: 3vw;  margin: 0 auto 50px; font-weight: 300; ">
					{{ NAME }} ({{ EMAIL }})
				</h2>
				<p style="width: 94%; max-width: 350px;  color: #646479; margin: 0 auto 25px; font-size: 15px; ">
					Phone {{ PHONE }}
				</p>
				<p style="width: 94%; max-width: 350px;  color: #646479; margin: 0 auto 25px; font-size: 15px; ">
					{{ SERVICES }}
				</p>
			</td>
		</tr>
	</table>
</body>
</html>
