$(document).ready(function() {

	$('.pricing-item input').change(function(){
		var priceValue = $('.pricing-item input:checked').siblings('label').find('strong').text(),
				calculated = 0,
				checkedInputLength = $('.pricing-item input:checked').length;

		$('#checked-input-length').html(checkedInputLength);

		priceValue.split('$').filter(function(i) {
			var round = Math.round(i);
			calculated = round + calculated;
			$('#pricing-form-calc').find('strong').html('$' + calculated);
		});

	});

/*
	(function() {

		$('.pricing-select-all-checkbox input').on('change', function() {
			$('.pricing-checkbox').prop('checked', this.checked)
			if ( $('.pricing-checkbox').is(':checked') ) {
				$('.pricing-order-service-btn').removeClass('disabled');
			} else {
				$('.pricing-order-service-btn').addClass('disabled');
			}
		});

		$('.pricing-checkbox').on('change', function() {
			if ( $('.pricing-checkbox').is(':checked') ) {
				$('.pricing-order-service-btn').removeClass('disabled');
			} else {
				$('.pricing-order-service-btn').addClass('disabled');
			}
		});

	})();
*/

   /*scrollToLink*/
	$('a[href^="#"], a[href^="."]').click( function(){ // если в href начинается с # или ., то ловим клик
	   var scroll_el = $(this).attr('href'); // возьмем содержимое атрибута href
       if ($(scroll_el).length != 0) { // проверим существование элемента чтобы избежать ошибки
	   $('html, body').animate({ scrollTop: $(scroll_el).offset().top }, 800); // анимируем скроолинг к элементу scroll_el
       }
	   return false; // выключаем стандартное действие
   });

});