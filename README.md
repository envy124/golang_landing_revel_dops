# Welcome to Revel

A high-productivity web framework for the [Go language](http://www.golang.org/).

### Dependencies:
    
    Storage - mysql on localhost:6603
    Login root
    Password 1234
    Configurable via dops/conf/app.conf:102

### Installation:

    1. go get github.com/revel/revel
    2. go get github.com/revel/cmd/revel
    3. git clone https://github.com/logpacker/dops $GOPATH/src/dops

### Start the web server:

   revel run dops
   
   golang dependencies will be downloaded by revel automatically, but in some cases may be useful install them with glide:
   cd $GOPATH/src/dops (or your location)
   glide install

### Go to http://localhost:9000/ and you'll see index page

## Code Layout

The directory structure of a generated Revel application:

    conf/             Configuration directory
        app.conf      Main app configuration file
        routes        Routes definition file

    app/              App sources
        init.go       Interceptor registration
        controllers/  App controllers go here
        views/        Templates directory

    messages/         Message files

    public/           Public static assets
        css/          CSS files
        js/           Javascript files
        images/       Image files

    tests/            Test suites


## Help

* The [Getting Started with Revel](http://revel.github.io/tutorial/gettingstarted.html).
* The [Revel guides](http://revel.github.io/manual/index.html).
* The [Revel sample apps](http://revel.github.io/examples/index.html).
* The [API documentation](https://godoc.org/github.com/revel/revel).

