package controllers

import (
	"dops/app/models"
	"fmt"
	"github.com/logpacker/mailer/pkg/client"
	"github.com/revel/revel"
	"io/ioutil"
	"path"
	"strings"
)

var (
	Tests           bool
	MailerDir       string
	FromNameMailer  = "DOps"
	FromEmailMailer = "mailer@weelco.com"
	MailerClient    *client.Client
)

type Mailer struct {
	*revel.Controller
}

func (m Mailer) Unsubscribe(code string) revel.Result {
	return m.RenderJSON("not implemented")
}

func GetHost() string {
	return strings.TrimRight(
		revel.Config.StringDefault("http.host", "http://localhost"), "/")
}

func SendContact(c models.Contact, moderators []models.Moderator) error {
	params := map[string]string{
		"SUBJECT": "DOps contact",
		"NAME":    c.Name,
		"EMAIL":   c.Email,
		"WEBSITE": c.Website,
		"COUNTRY": c.Country,
		"PHONE":   c.Phone,
		"NOTE":    c.Note,
	}
	for _, m := range moderators {
		if err := Send(FromEmailMailer, FromNameMailer, m.Email, "contact", params); err != nil {
			return err
		}
	}
	return nil
}

func SendOrder(so models.Order, services []models.Service, moderators []models.Moderator) error {
	var servicesStr string
	for _, s := range services {
		servicesStr += fmt.Sprintf("Name: %s<br>Price: %.2f<br>", s.Name, s.Price)
	}
	params := map[string]string{
		"SUBJECT":  "DOps order",
		"NAME":     so.Name,
		"EMAIL":    so.Email,
		"PHONE":    so.Phone,
		"SERVICES": servicesStr,
	}
	for _, m := range moderators {
		if err := Send(FromEmailMailer, FromNameMailer, m.Email, "order", params); err != nil {
			return err
		}
	}
	return nil
}

func Send(fromEmail, fromName, email, template string, replace map[string]string) error {
	if Tests {
		return nil
	}

	subject := ""
	if _, ok := replace["SUBJECT"]; ok {
		subject = replace["SUBJECT"]
	}
	replace["UNSUBSCRIBE_URL"] = GetHost() + "/unsubscribe?code=123"

	body, bodyErr := processTemplate(template, email, replace)
	if bodyErr != nil {
		return bodyErr
	}

	return MailerClient.SendEmail(client.Email{
		From: &client.Address{
			Email: fromEmail,
			Name:  fromName,
		},
		To: &client.Address{
			Email: email,
		},
		Subject:        subject,
		Body:           body,
		URLUnsubscribe: replace["UNSUBSCRIBE_URL"],
	})
}

// processTemplate replaces placeholders in Template
func processTemplate(template string, email string, replace map[string]string) (string, error) {
	letterBytes, err := ioutil.ReadFile(path.Join(MailerDir, template+".tpl"))
	if err != nil {
		return "", err
	}

	letter := string(letterBytes)
	// letter = strings.Replace(letter, "{{ CDN_HOST }}", common.GetRandomCDNHost(), -1)
	// letter = strings.Replace(letter, "{{ WEB_ROOT }}", common.Config.WebRoot, -1)

	for replaceKey, replaceValue := range replace {
		letter = strings.Replace(letter, "{{ "+replaceKey+" }}", replaceValue, -1)
	}

	return letter, nil
}
